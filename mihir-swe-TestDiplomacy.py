import unittest
from Diplomacy import diplomacy_solve

class TestDiplomacy(unittest.TestCase):
    def test_example_1(self):
        data = [
            "A Madrid Hold",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Madrid"])

    def test_example_2(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B Madrid", "C London"])

    def test_example_3(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]"])

    def test_example_4(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])

    def test_example_5(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]"])

    def test_example_6(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B Madrid", "C [dead]", "D Paris"])

    def test_example_7(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
            "E Austin Support A",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])

if __name__ == "__main__":
    unittest.main()
